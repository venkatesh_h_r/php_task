<?php

$url = 'http://hradmin.aryupay.io/tracking/viewreport.php'; // path to your JSON file
$data = file_get_contents($url); // put the contents of the file into a variable
$characters = json_decode($data);

$myfile = fopen('data.json', 'w');
fwrite($myfile, $data);
// echo "<pre>";
// print_r($characters);
// echo "</pre>";

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>PHP task</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
  
</head>
<body>

<div class="container">
  <h2>PHP Task</h2>
  <p></p>
  <table id="example" class="table table-dark table-striped">
    <thead>
      <tr>
        <th>id</th>
		<th>name</th>
		<th>category</th>
		<th>categoryid</th>
		<th>address</th>
		<th>description</th>
		<th>contact</th>
		<th>empcode</th>
		<th>image</th>
		<th>Action</th>
      </tr>
    </thead>
    <tbody>
     <?php
	foreach($characters->Success as $data) { ?>
	<tr>
		<td><?php echo $data->id; ?></td>
		<td><?php echo $data->name; ?></td>
		<td><?php echo $data->category; ?></td>
		<td><?php echo $data->categoryid; ?></td>
		<td><?php echo $data->address; ?></td>
		<td><?php echo $data->description; ?></td>
		<td><?php echo $data->contact; ?></td>
		<td><?php echo $data->empcode; ?></td>
		<!-- <td><?php echo $data->image; ?></td> -->
		<td><img src="<?php echo $data->image; ?>" width="100px" height="100px"></td>
		<td><a href="#" id="<?php echo $data->id; ?>" class="edit"><i class="fas fa-edit"></i></a></td>
		<!-- <td><a href="#" data-toggle="modal" data-target="#ModalExample" class="edit"><i class="fas fa-edit"></i></a></td> -->
		
	</tr>
	<?php
        }
    ?>
    </tbody>
  </table>


</div>
<div id="ModalExample" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-xs-center">Edit</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="POST" action="">
                    <input type="hidden" name="_token" value="">
                    <div class="form-group">
                        <label class="control-label">name</label>
                        <div>
                            <input type="text" class="form-control input-lg" name="name" id="name" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">category</label>
                        <div>
                            <input type="text" class="form-control input-lg" name="category" id="category" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">category id</label>
                        <div>
                            <input type="text" class="form-control input-lg" name="categoryid" id="categoryid" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">address</label>
                        <div>
                            <input type="text" class="form-control input-lg" name="address" id="address" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">description</label>
                        <div>
                            <input type="text" class="form-control input-lg" name="description" id="description" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">contact</label>
                        <div>
                            <input type="text" class="form-control input-lg" name="contact" id="contact" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">image</label>
                        <div>
                            <input type="text" class="form-control input-lg" name="image" id="image" value="">
                        </div>
                    </div>
                    
                    
                    <div class="form-group">
                        <div>
                            <!-- <a class="btn btn-link" href="">Forgot Your Password?</a> -->
                            <button type="submit" class="btn btn-info btn-block update">Update</button>
                        </div>
                    </div>
                </form>
            </div>
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){

		$("#example").on("click",".edit",function(){
			var id = $(this).attr('id');
			$.getJSON('data.json', function(result){
		    	var json = result;
				var forum = json.Success;
			for (var i = 0; i < forum.length; i++) {
			    var object = forum[i];
			    if (object['id'] == id) {

			    	$('#name').val(object['name']);
			    	$('#category').val(object['category']);
			    	$('#categoryid').val(object['categoryid']);
			    	$('#address').val(object['address']);
			    	$('#description').val(object['description']);
                    $('#contact').val(object['contact']);
                    $('#image').val("<img src='" + object['image'] + "' width='100px' height='100px'>");
			    	$('#ModalExample').modal('show');

			    	//  for (property in object) {
				    //     var value = object[property];
				    //     alert(property + "=" + value); // This alerts "id=1", "created=2010-03-19", etc..
				    // }
			    }
			    // alert(object['id']);
			   
			}
			
			
		  });
			

		});

    $('.update').click(function(){
      alert("update url needed");
    })



	})
</script>
</body>
</html>